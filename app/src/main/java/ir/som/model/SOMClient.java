package ir.som.model;


import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SOMClient {

    @GET("/SOMAPI/objectsall")
    Call<AllObjectsResponse> getAllObjects();

    @GET("/SOMAPI/info/")
    Call<Object> getObjectInfo(@Query("id") String id);

    @GET("/SOMAPI/all")
    Call<AllStatusResponse> getAllStatus();

    @POST("/SOMAPI/change")
    @FormUrlEncoded
    Call<ResponseBody> change(@Field("id") String id , @Field("status") String status , @Field("last_staff") String last_staff);
}

