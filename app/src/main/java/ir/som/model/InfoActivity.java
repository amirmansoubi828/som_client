package ir.som.model;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;

import ir.som.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoActivity extends AppCompatActivity {

    private Object object;
    private String[] statusList;
    EditText id, staff;
    Spinner status;
    TextView time;
    Button change;
    SOMClient somClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);



        id = (EditText) findViewById(R.id.info_edittext_id);
        status = (Spinner) findViewById(R.id.info_status_spinner);
        staff = (EditText) findViewById(R.id.info_edittext_staff);
        time = (TextView) findViewById(R.id.info_textview_time);

        object = new Object();


        object.setId(onNewIntentListener(getIntent()));
        id.setText(object.getId());

        somClient = RetrofitManager.createService();
        Call<AllStatusResponse> allStatus = somClient.getAllStatus();
        allStatus.enqueue(new Callback<AllStatusResponse>() {
            @Override
            public void onResponse(Call<AllStatusResponse> call, Response<AllStatusResponse> response) {
                statusList = response.body().status.toArray(new String[response.body().status.size()]);
                status.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, statusList));
            }

            @Override
            public void onFailure(Call<AllStatusResponse> call, Throwable t) {
                Toast.makeText(InfoActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        Call<Object> objectInfo = somClient.getObjectInfo(object.getId());
        objectInfo.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                object = response.body();
                int status_position = 0;
                try {
                    for (int i = 0; i < statusList.length; i++) {
                        if (object.getStatus().equals(statusList[i])) {
                            status_position = i;
                            break;
                        }
                    }
                    status.setSelection(status_position);}
                catch (Exception e){
                    e.printStackTrace();
                }
                staff.setText(object.getLast_staff());
                time.setText("Last Edit : " + object.getLast_edit_time());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(InfoActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        change = (Button) findViewById(R.id.info_button_change);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                object.setStatus(status.getSelectedItem().toString());
                object.setLast_staff(staff.getText().toString());
                object.setLast_edit_time("");
                Call<ResponseBody> call = somClient.change(object.getId(), object.getStatus(), object.getLast_staff());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Toast.makeText(InfoActivity.this, response.body().string(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            Toast.makeText(InfoActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(InfoActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });


    }

    protected String onNewIntentListener(Intent intent) {
        Toast.makeText(this, "new Intent", Toast.LENGTH_SHORT).show();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] rawMessages =
                    intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMessages != null) {
                NdefMessage[] messages = new NdefMessage[rawMessages.length];
                for (int i = 0; i < rawMessages.length; i++) {
                    messages[i] = (NdefMessage) rawMessages[i];
                    for (int j = 0; j < messages[i].getRecords().length; j++) {
                        return new String(messages[i].getRecords()[i].getPayload()).substring(3);
                    }
                }
            }
        }
        return new String("0");
    }
}
