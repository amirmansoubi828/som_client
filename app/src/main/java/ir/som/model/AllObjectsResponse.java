package ir.som.model;

import java.util.ArrayList;

public class AllObjectsResponse {
    ArrayList<Object> list;

    public AllObjectsResponse(ArrayList<Object> list) {
        this.list = list;
    }

    public ArrayList<Object> getList() {
        return list;
    }
}
