package ir.som.model;

import java.util.ArrayList;

public class AllStatusResponse {
    ArrayList<String> status ;

    public ArrayList<String> getStatus() {
        return status;
    }

    public void setStatus(ArrayList<String> status) {
        this.status = status;
    }
}
