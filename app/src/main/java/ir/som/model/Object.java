package ir.som.model;

public class Object {
    private String id;
    private String status;
    private String last_staff;
    private String last_edit_time;

    public String getLast_edit_time() {
        return last_edit_time;
    }

    public void setLast_edit_time(String last_edit_time) {
        this.last_edit_time = last_edit_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLast_staff() {
        return last_staff;
    }

    public void setLast_staff(String last_staff) {
        this.last_staff = last_staff;
    }
}
