package ir.som.model;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitManager {


    public static SOMClient createService() {
        String SOM_SERVER="http://192.168.43.187:8000/";

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(SOM_SERVER)
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        return retrofit.create(SOMClient.class);
    }
}
