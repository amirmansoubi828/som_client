package ir.som;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ir.som.model.AllObjectsResponse;

public class MainActivity extends AppCompatActivity {

    Button new_btn, info_btn, all_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new_btn = (Button) findViewById(R.id.main_new_button);
        info_btn = (Button) findViewById(R.id.main_info_button);
        all_btn = (Button) findViewById(R.id.main_all_button);

        all_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AllObjectsActivity.class));
            }
        });


    }
}
