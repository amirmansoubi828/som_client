package ir.som;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ir.som.model.AllObjectsResponse;
import ir.som.model.Object;
import ir.som.model.RetrofitManager;
import ir.som.model.SOMClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllObjectsActivity extends AppCompatActivity {
    private ListView listView;
    private TextView textView;
    private SOMClient somClient;
    private ArrayList<Object> objectArrayList;
    private Map<String, Integer> statusDictionary;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_objects);
        Toast.makeText(this, "up", Toast.LENGTH_SHORT).show();
        listView = (ListView) findViewById(R.id.all_objects_listview);
        textView = (TextView) findViewById(R.id.all_objects_textview);
        somClient = RetrofitManager.createService();
        Toast.makeText(this, "ret up", Toast.LENGTH_SHORT).show();
        Call<AllObjectsResponse> responseCall = somClient.getAllObjects();
        responseCall.enqueue(new Callback<AllObjectsResponse>() {
            @Override
            public void onResponse(Call<AllObjectsResponse> call, Response<AllObjectsResponse> response) {
                objectArrayList = new ArrayList<>();
                objectArrayList = response.body().getList();
                statusDictionary = new HashMap<String, Integer>();
                for (Object obj :
                        objectArrayList) {
                    if (statusDictionary.containsKey(obj.getStatus())) {
                        statusDictionary.put(obj.getStatus(), statusDictionary.get(obj.getStatus()) + 1);
                    } else {
                        statusDictionary.put(obj.getStatus(), 1);
                    }
                }
                for (String status :
                        statusDictionary.keySet()) {
                    textView.append(status + " (" + statusDictionary.get(status) + ")\n");
                }
                listView.setAdapter(new ArrayAdapter<Object>(getApplicationContext(), R.layout.seli2, android.R.id.text1, objectArrayList.toArray(new Object[objectArrayList.size()])) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                        TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                        text1.setText("ID : " + objectArrayList.get(position).getId());
                        text2.setText("Status : " + objectArrayList.get(position).getStatus() + "\nEdited by : " + objectArrayList.get(position).getLast_staff() + "\nAt : " + objectArrayList.get(position).getLast_edit_time());
                        return view;

                    }
                });
            }

            @Override
            public void onFailure(Call<AllObjectsResponse> call, Throwable throwable) {
                Toast.makeText(AllObjectsActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
